from .core import get_dataset_names, load_dataset


__version__ = "0.1.1"
